#!/usr/bin/python

# A data packet
# chunk size - (32 bits) informs receiver of chunk payload size, i.e.
#              how many bytes of data will follow the header
# stream offset - (64 bits) indicates position of chunk within the
#              stream, used to copy the chunk in the right position of
#              application buffer || for now, use as chunk index
# block size - (32 bits) allows receiver to check if the block can fit in
#              the app buffer, if too large preallocate addtnl temp buffer
# block index - (32 bits) used to verify that chunk belongs to the block
#			   currently being received
# data in bytes? 
#
class Packet:
	def __init__(self, chunk_size, stream_offset, block_size, block_index, data_chunk):
		self._chunk_size = chunk_size
		self._stream_offset = stream_offset
		self._block_size = block_size
		self._block_index = block_index
		self._data_chunk = data_chunk
	
	def get_chunk_size(self):
		return self._chunk_size
	
	def get_stream_offset(self):
		return self._stream_offset
		
	def get_block_size(self):
		return self._block_size
		
	def get_block_index(self):
		return self._block_index
		
	def get_data_chunk(self):
		return self._data_chunk
