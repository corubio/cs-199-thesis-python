#!/usr/bin/python

import errno
import time
import mmap
import Config
import sys
import struct
from Interface import *
from Connection import *
from Packet import *

# --------------------------------------------------
# Global variables
# --------------------------------------------------
_addr_list = get_addresses()
_conn_list = []
_dlvr_packet_list = []
_rtrv_packet_list = []
_bytes_list = [] #list of byte array
_rcvd_messages = []
_conn_counter = 0
_status = False
_ack = False
_rtt_s = None

class Shim:
    # ---------------------------------------------    
    # PUBLIC methods
    # ---------------------------------------------
    
    @staticmethod
    def get_status():
        return _status
    
    ## Called to run as server, creates a server socket and waits for client connection
    #
    @staticmethod
    def run_as_server(IP, port):
              
        sock = Shim._create_server_socket(IP, port)                        
        Shim._add_connection(sock, IP, None, 1)
        
        return sock            
    
    ## Called to run as client, creates a client socket and connects to server
    #
    @staticmethod    
    def run_as_client(IP, port):
        
        sock = Shim._create_client_socket(IP, port)
        Shim._connect_to_server(sock, IP, port)
    
    ## Call to send a file
    #
    @staticmethod        
    def send_file(filename):
        global _ack 
        _ack = False
        
        
        #print("Sending file...")
        Shim._convert_to_packets(filename)
        #print ("Packets: " + str(len(_dlvr_packet_list)))
        count = 0
        conn_size = len(_conn_list)
        #start = time.time()
        
        Shim.compute_connection_rtt()
        Shim._assign_pckt_to_conn()
        
        '''
        for packet in _dlvr_packet_list:
            #print packet.get_stream_offset()
            #print packet.get_data_chunk()
            #Shim._dlvr_packet(_conn_list[count%conn_size], packet)
            conn = count%conn_size
            #print (conn)
            #start = time.time()
            bytes = Shim._convert_packet_to_bytes(packet)
            
            #end = time.time()
            
            #print("Time elapsed after packet conversion: " + str(end - start))
            #print ("bytes size: " + str(len(bytes)))
            #start = time.time()
            #break
            #result = _conn_list[conn].get_socket().connect_ex(_conn_list[conn].get_socket().getsockname())
                
            _conn_list[conn].send_msg(bytes)
            #end = time.time()
            #print("Time elapsed between packet sending: " + str(end - start))
            #start = time.time()
            #_conn_list[conn].send_msg("PKTSENT")
            
            #print (_conn_list[conn].get_src_IP() + " " + _conn_list[conn].get_dst_IP())
            
            #Shim.send_msg(Shim._convert_packet_to_bytes(packet))
            #while True:
            #time.sleep(0.5)
            
            count = count + 1    
        '''    
        Shim.send_msg("END")        
    
    ## call to send msg
    #
    @staticmethod
    def send_msg(msg):
        print ('Sending Message: \n' + msg)
        global _conn_list
        for conn in _conn_list:
            if conn:
                #conn.send_msg("MSG::" + msg)        
                conn.send_msg(msg)
                print ("Message sent.")
                break
        
    ## Call to recv message
    #
    @staticmethod
    def recv_msg():
        return Shim._get_msg()
    
    ## Call to recv file
    #
    @staticmethod
    def recv_file():
        global _rtrv_packet_list
        packet = Shim._convert_bytes_to_packet(Shim._get_msg()) 
        #print(packet.get_stream_offset())
        block_size = packet.get_block_size()
        chunk_size = packet.get_chunk_size()
        _rtrv_packet_list.append(packet)
        while chunk_size < block_size:
            packet = Shim._convert_bytes_to_packet(Shim._get_msg())         
            #print(packet.get_stream_offset())
            chunk_size = chunk_size + packet.get_chunk_size()   
            _rtrv_packet_list.append(packet)   
        
        try: import operator
        except ImportError: keyfun= lambda x: x.get_stream_offset() # use a lambda if no operator module
        else: keyfun= operator.attrgetter("_stream_offset")
        
        _rtrv_packet_list.sort(key=keyfun, reverse=False)
        return _rtrv_packet_list
        
    ## Called by Connection class to deliver message to Shim
    #
    @staticmethod
    def rtrv_msg(msg):
        global _rcvd_messages
        global _status
        global _rtt_s
        '''
        if msg[0:3] == "END":
            #print ("status is true")
            _status = True
        elif msg[0:2] == "OK":
            #print ("msg is ok")
            _ack = True
        else:
            _rcvd_messages.append(msg)
        '''
        if msg[0:3] == "END":
            _status = True
        elif msg[0:6] == "OKRTT:":
            rtt = msg.split(":::")[0]
            _rtt_s = float(rtt.split(" ")[1])
        else:
            _rcvd_messages.append(msg.split(":::")[0])    
            #print ("Received packet!")
            #packet = Shim._convert_bytes_to_packet(Shim._get_msg())
            #print packet.get_stream_offset()
            #print (msg[0:5])
            #print ("Num of received packets: " + str(len(_rcvd_messages)))       
        #print ("Received messaged: " + str(len(_rcvd_messages)))
        #print("Message: " + msg)
    
    ## called by client to check if server is multipath capable, creates connections to received addresses from server if yes
    #
    @staticmethod
    def check_multipath():
        print ("Check for multipath.")
        
        Shim.send_msg("CHK MP_CAPABLE")
        
        msg = Shim.recv_msg().split(" ", 1)

        if msg[0] == "CPB":
            for conn in msg[1].split():
                addr_port = conn.split(":")
                Shim.run_as_client(addr_port[0], int(addr_port[1]))
            
            return True
        else:
            return False        
    
    ## Called by server to verify multipath capability
    #
    @staticmethod                
    def verify_multipath():
        print ("Verify multipath.")
        
        addr_port_list = ""
        
        global _addr_list
        for addr in _addr_list:
            #print ("addr: " + addr)
            if Shim._is_connected(addr) == True:
                sock = Shim.run_as_server(addr, 0)
                addr_port = sock.getsockname()
                #print ("addr_port: " + str(addr_port))
                addr_port_list = addr_port_list + " " + addr_port[0] + ":" + str(addr_port[1])
                
        Shim.send_msg("CPB" + addr_port_list) 
    
    ## Computes and sets the RTT and pckt count for each connection and sorts conn_list by rtt time, slowest -> fastest
    # 
    @staticmethod
    def compute_connection_rtt():
        #print ("")
        #print ("Compute RTT conn")
        global _conn_list
        
        for conn in _conn_list:
            rtt = Shim._get_rtt(conn)
            conn.set_rtt(rtt)
            #print conn.get_id()            
        
        try: import operator
        except ImportError: keyfun= lambda x: x.get_rtt() # use a lambda if no operator module
        else: keyfun= operator.attrgetter("_rtt")
        
        _conn_list.sort(key=keyfun, reverse=True)
        
        _conn_list[0].set_pckt_count(1)
        max_rtt = _conn_list[0].get_rtt()
        
        for i in range(1, len(_conn_list)):
            rtt = _conn_list[i].get_rtt()
            
            pckt_count = int(-(-max_rtt//rtt)) #ceiling division
            _conn_list[i].set_pckt_count(pckt_count)
        '''
        for conn in _conn_list:
            print (conn.get_rtt())
            print (conn.get_pckt_count())
        '''
        print ("done")    
    # --------------------------------------------
    # Util methods/private methods
    # --------------------------------------------
    
    @staticmethod
    def _assign_pckt_to_conn():
        global _conn_list
        global _dlvr_packet_list
        
        while len(_dlvr_packet_list) > 0:
            for conn in _conn_list:
                for i in range(conn.get_pckt_count()):
                    if len(_dlvr_packet_list) == 0:
                        break
                    bytes = Shim._convert_packet_to_bytes(_dlvr_packet_list.pop(0))
                    conn.send_msg(bytes)
    
    ## Computes the RTT for a connection: Returns rtt
    #
    @staticmethod
    def _get_rtt(conn):
        #print ("Get rtt")
        
        conn.send_msg("RTT: " + str(time.time()))
        s_time = Shim._get_rtt_msg()
        e_time = time.time()
        rtt = e_time - s_time
        
        return rtt
    
    @staticmethod
    def _is_connected(IP):
        print ("check if connected: " + IP)
        for conn in _conn_list:
            if conn.get_src_IP() == IP:
                print ("already connected")
                return False
                
        return True    
        
    ## Takes IP address and port: 0 as arguments ans returns socket
    #
    @staticmethod
    def _create_server_socket(IP, port):
        sock = None
        try:
            # Create a TCP/IP socket
            print >> sys.stderr, 'S: Creating TCP/IP socket...'
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print >> sys.stderr, 'S: SUCCESS.'
            
            # Bind socket to port
            address = (IP, port)
            sock.bind(address)
            print >> sys.stderr, 'S: Starting up on %s port %s...' % sock.getsockname() 
            print >> sys.stderr, 'S: SUCCESS.'    
            
        except Exception as msg:
            print "Caught exception Socket error : %s" % msg
            
        return sock
    
    ## Takes in server IP and port and creates client socket
    #
    @staticmethod
    def _create_client_socket(IP, port):
        try:
            # Create a TCP/IP socket
            print >> sys.stderr, 'S: Creating TCP/IP socket...'
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            
            print >> sys.stderr, 'S: SUCCESS.'
        except Exception as msg:
            print "Caught exception Socket error : %s" % msg    
        
        return sock
    
    ## Client socket connects to server
    #
    @staticmethod            
    def _connect_to_server(sock, IP, port):
        try:
            server_address = (IP, port)
            print >> sys.stderr, 'Connecting to %s port %s...' % server_address
            sock.connect(server_address)
            print >> sys.stderr, 'SUCCESS.'
        except Exception as msg:
            print "Caught exception Socket error : %s" % msg    
        Shim._add_connection(sock, sock.getsockname()[0], IP, 0)
    
    ## Creates a connection and adds it to conn_list
    #
    @staticmethod    
    def _add_connection(sock, src, dst, conn_type):
        global _conn_counter
        global _conn_list
        ID = _conn_counter
        
        conn = Connection(ID, sock, src, dst, conn_type)
        _conn_list.append(conn)
        conn.start()
        
        print ("Connection List: " + str(len(_conn_list)))
        _conn_counter = _conn_counter + 1
        
        
    ## Assign packet sending to chosen connection
    #
    @staticmethod
    def _dlvr_packet(conn, packet):
        conn.send_msg(Shim._convert_packet_to_bytes(packet))    
    
    ## Helper function for receiving message
    #
    @staticmethod    
    def _get_msg():
        global _rcvd_messages
        while True:
            if len(_rcvd_messages) != 0:
                break
   
        msg = _rcvd_messages[0]
        _rcvd_messages.pop(0)
        return msg                
    
    @staticmethod
    def _get_rtt_msg():
        global _rtt_s
        while True:
            if _rtt_s != None:
                break        
        
        msg = _rtt_s
        _rtt_s = None
        return msg
    
    ## To check if there's an acknowledgement that msg has been recvd
    #
    @staticmethod
    def _get_ack():
        global _ack
        while True:
            if _ack == True:
                break
        _ack = False
        return True 
    
    ## Accepts string filename and converts file in bytes to packets
    #
    @staticmethod
    def _convert_to_packets(filename):
        global _dlvr_packet_list
        
        bytes = Shim._read_file_as_bytes(filename)
        size = len(bytes)
        
        chunk_size = Config.CHUNK_SIZE
        stream_offset = 0
        block_size = len(bytes)    
        block_index = 0    
        
        while stream_offset < block_size:
            end = min(stream_offset+chunk_size, block_size)
            data_chunk = bytes[stream_offset:end]            
            _dlvr_packet_list.append(Packet(chunk_size, stream_offset, block_size, block_index, data_chunk))
            stream_offset = stream_offset + chunk_size            
        
    ## Accepts string filename as argument and returns read file in byte array
    #
    @staticmethod
    def _read_file_as_bytes(filename):
        try:
            f = open(filename, "rb")
            m = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
            bytes = bytearray(m)   
            return bytes 
        except IOError as e:
            print ("I/O error({0}): {1}".format(e.errno, e.strerror))
        except:
            print ("Unexpected error:", sys.exc_info()[0])         
         
    ## Converts a packet object to byte array 
    #
    @staticmethod
    def _convert_packet_to_bytes(packet):
        chunk_size = Shim._convert_int_to_4bytes(packet.get_chunk_size())
        stream_offset = Shim._convert_int_to_8bytes(packet.get_stream_offset())
        block_size = Shim._convert_int_to_4bytes(packet.get_block_size())
        block_index = Shim._convert_int_to_4bytes(packet.get_block_index())
        bytes = bytearray()
        bytes.extend(chunk_size)
        bytes.extend(stream_offset)
        bytes.extend(block_size)
        bytes.extend(block_index)
        bytes.extend(packet.get_data_chunk())
        return bytes
    
    # Converts byte array to a packet object
    #    
    @staticmethod
    def _convert_bytes_to_packet(bytes):
        chunk_size = Shim._convert_4bytes_to_int(bytes[0:4])
        stream_offset = Shim._convert_8bytes_to_int(bytes[4:12])
        block_size = Shim._convert_4bytes_to_int(bytes[12:16])
        block_index = Shim._convert_4bytes_to_int(bytes[16:20])
        data_chunk = bytes[20:len(bytes)]
        
        return Packet(chunk_size, stream_offset, block_size, block_index, data_chunk)        

    ## Functions for integer to bytes, bytes to integer conversion 
    #
    
    @staticmethod
    def _convert_4bytes_to_int(bytes):
        return int(struct.unpack("!I", bytes)[0])
    
    @staticmethod
    def _convert_8bytes_to_int(bytes):
        return int(struct.unpack("!Q", bytes)[0])
    
    @staticmethod    
    def _convert_int_to_4bytes(num):
        return bytearray(struct.pack("!I", num))
    
    @staticmethod    
    def _convert_int_to_8bytes(num):
        return bytearray(struct.pack("!Q", num))  
