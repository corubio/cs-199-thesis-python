from Shim import *

#shim = Shim()

Shim.run_as_server("10.0.0.1", 0)

while True: 
    
    msg = Shim.recv_msg()
    #print ("MSG: " + msg)
    
    if msg[0:14] == "CHK MP_CAPABLE":
        Shim.verify_multipath()
    
    elif msg[0:3] == "GET":
        print msg
        filename = msg.split(" ")[1]
        
        Shim.send_file(filename)    
        
