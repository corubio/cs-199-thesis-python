#!/usr/bin/python

import threading
import socket
import sys
import Config 
import time

class Connection(threading.Thread):
    def __init__(self, ID, sock, src, dst, conn_type):
        threading.Thread.__init__(self)
        self._id = ID
        self._socket = sock
        self._src = src
        self._dst = dst
        self._server = conn_type
        self._rtt = None
        self._pckt_count = None
    
    def get_src_IP(self):
        return self._src
        
    def get_dst_IP(self):
        return self._dst
    
    def get_socket(self):
        return self._socket
    
    def set_rtt(self, rtt):
        self._rtt = rtt
        
    def get_rtt(self):
        return self._rtt
        
    def set_pckt_count(self, count):
        self._pckt_count = count
        
    def get_pckt_count(self):
        return self._pckt_count
        
    def send_msg(self, msg):
        size = len(msg)
        if size < 1500:
            #t_msg = msg.split("::")[1]
            #msg = t_msg
            msg = msg + ":::"
            msg_ba = bytearray(msg)
            length = len(msg_ba)
            #print ("length: " + str(length))
            padding = 1500-(length)
            #print ("padding: " + str(padding))
            msg_ba.extend(bytearray(1*padding))
            #print ("send msg size: " + str(len(msg_ba)))
            self._socket.sendall(msg_ba)
        else:
            #print ("send msg size: " + str(size))
            self._socket.sendall(msg)
    
    def recv_msg(self):
        data = self._socket.recv(Config.MTU, socket.MSG_WAITALL)
        return data
        
    def close(self):
        self._socket.close()
    
    def run(self):
        from Shim import *
         
        if self._server == 1:
            self._socket.listen(1)                
            print >> sys.stderr, 'Waiting for a connection...'
            connection, client_address = self._socket.accept()
            print >> sys.stderr, 'S: SUCCESS.\nConnection from ', client_address
            self._socket = connection
            self._dst = client_address[0]
        
        #count = 0 
        #start = time.time()   
        while 1:
            msg = self.recv_msg()
            if msg:
                #end = time.time()
                #print("Time elapsed between packet recv: " + str(end - start))
                #start = time.time()
                #print ("rcvd msg size: " + str(len(msg)))
                #print msg
                #count = count + 1
                #if len(msg) == 3:
                #    print msg
                '''
                if msg == "PKTSENT":
                    self.send_msg("OK")
                else:
                    Shim.rtrv_msg(msg)
                '''
                if msg[0:4] == "RTT:":
                    self.send_msg("OK" + msg.split(":::")[0])
                elif msg[0:6] == "OKRTT:":
                    Shim.rtrv_msg(msg)
                else:    
                    Shim.rtrv_msg(msg)
                #if msg[0:2] != "OK":
                #    self.send_msg("OK")
                #print (count)
                
            # do  stuff to msg
