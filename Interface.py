#!/usr/bin/python

import netifaces

# A network interface 
# addr_list - an interface can have more than one address
#
class Interface:
	def __init__(self, name, AF_INET_list):
		self.__name = name
		self.__AF_INET_list = AF_INET_list
		self.__num_addr = len(AF_INET_list)
		self.__addr_list = []

		# get all available addresses
		for AF_INET in AF_INET_list:
			self.__addr_list.append(AF_INET['addr'])

	
	def get_name(self):
		return self.__name

	def get_AF_INET_list(self):
		return self.__AF_INET_list
	
	def get_addr_list(self):
		return self.__addr_list
	
	# get an address, specify index
	def get_addr(self, i):
		return self.__addr_list[i]

	def get_num_addr(self):
		return self.__num_addr

	# get netmask of an address using the index of the addr
	def get_netmask_at(self, i):
		return self.__AF_INET_list[i]['netmask']

	# get netmask of an address
	def get_netmask(self, addr):
		for AF_INET in self.__AF_INET_list:
			if (AF_INET['addr'] == addr):
				return AF_INET['netmask']		

	### add other necessary functions ###		

# Returns list of host's available interfaces with IPv4 address
#
def get_interfaces():
	interface_list = netifaces.interfaces()
	Interface_list = []

	# For every interface with IPv4 address, create an instance of class Interface and add to Interface_list 
	#
	for interface in interface_list:
		AF_INET = netifaces.ifaddresses(interface).get(netifaces.AF_INET)
			
		if (AF_INET != None and
			interface  != 'lo'):
			anInterface = Interface(interface, AF_INET)
			Interface_list.append(anInterface)
	
	return Interface_list

# Returns list of host's available addresses
#	
def get_addresses():
	interface_list = get_interfaces()
	return_list = []
	for interface in interface_list:
		addr_list = interface.get_addr_list()
		for addr in addr_list:
			return_list.append(addr)
	
	return return_list
